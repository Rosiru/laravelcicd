#!/bin/sh
php composer.phar dump-autoload
php artisan config:cache
chown -R root:www-data /var/www/html/public/uploads
chmod -R 775 /var/www/html/public/uploads
service cron start
apache2-foreground
